fmt:
    cd rust && cargo +nightly fmt
    cd libs && just fmt

fmt-check:
    cd rust && cargo +nightly fmt --check
    cd libs && just fmt-check
