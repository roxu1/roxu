/**
 * # Roxu Header
 *
 * Roxu is a program for creating bindings between languages.
 * This header just contains typedef glue to aid the use of C
 * with roxu. Roxu is not designed for C but merely uses it as
 * an intermediary.
 *
 * Compatibility:
 *   - This file should be compatible with C11
 *   - roxu::primitives::{i128, u128} is only available on new GCC versions
 *     and as such will give a warning if it is not available.
 *
 *     By the same token when are those two types ever actually used?
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

/*
 * roxu::primitives
 *
 * Houses primitive datatypes
 */

/* Unsigned Integer Types */

typedef uint8_t roxu__primitives__u8;
typedef uint16_t roxu__primitives__u16;
typedef uint32_t roxu__primitives__u32;
typedef uint64_t roxu__primitives__u64;

#ifdef __SIZEOF_INT128__
typedef unsigned __int128 roxu__primitives__u128;
#else
#warning __SIZEOF_INT128__ not defined, removing roxu::primitives::{u128, i128}!
#endif

/* Signed Integer Types */

typedef int8_t roxu__primitives__i8;
typedef int16_t roxu__primitives__i16;
typedef int32_t roxu__primitives__i32;
typedef int64_t roxu__primitives__i64;

#ifdef __SIZEOF_INT128__
typedef __int128 roxu__primitives__i128;
#endif

/* Float types */

typedef float roxu__primitives__f32;
typedef double roxu__primitives__f64;

/* Boolean types */

typedef bool roxu__primitives__bool;

/* Void */
typedef roxu__primitives__u8 roxu__primitives__void;

/*
 * roxu::llt
 *
 * Houses low level types
 */

typedef size_t roxu__llt__usize;
typedef ssize_t roxu__llt__isize;
typedef void *roxu__llt__ptr;
