#![deny(missing_docs)]
#![deny(clippy::missing_docs_in_private_items)]
#![doc = include_str!("../README.md")]

use std::fmt::Write;

use ident::write_ident;
use roxu_definitions::{AssociatedItem, Constant, Function, IdentType, Path, Trait, Type};

mod ident;

/// A wrapper round a function to have it written as a function pointer
pub struct FP<'a>(pub &'a Function);

/// Converts an object to C
pub trait ToC {
    /// Preforms the conversion
    ///
    /// Parameters:
    ///   - `c_buffer` - where the tokens should be written
    ///   - `path` - the roxu path of the currently referenced item,
    ///     used for the identifier of a function, struct, &c &c
    ///
    /// # Errors
    ///
    /// This function should fail only if there is a problem writing to the,
    /// buffer, structs implementing ToC should always be compatible with C
    fn to_c<W: Write>(&self, c_buffer: &mut W, path: &Path) -> Result<(), std::fmt::Error>;
}

impl ToC for Path {
    /// Converts the path into a C identifier
    ///
    /// This is done by joining segments by double `__` underscores.
    /// Any character which is not allowed by C is replaced by `Uxxxx` (the unicode number)
    fn to_c<W: Write>(&self, c_buffer: &mut W, _path: &Path) -> Result<(), std::fmt::Error> {
        for (i, segment) in self.0.iter().enumerate() {
            write_ident(segment, i == 0, c_buffer)?;

            // Write the doulbe underscore
            if i + 1 != self.0.len() {
                c_buffer.write_str("__")?;
            }
        }

        Ok(())
    }
}

impl ToC for Type {
    fn to_c<W: Write>(&self, c_buffer: &mut W, path: &Path) -> Result<(), std::fmt::Error> {
        self.path.to_c(c_buffer, path)
    }
}

impl ToC for IdentType {
    fn to_c<W: Write>(&self, c_buffer: &mut W, path: &Path) -> Result<(), std::fmt::Error> {
        self.typ.to_c(c_buffer, path)?;
        c_buffer.write_char(' ')?;
        write_ident(&self.name, true, c_buffer)
    }
}

impl ToC for Function {
    fn to_c<W: Write>(&self, c_buffer: &mut W, path: &Path) -> Result<(), std::fmt::Error> {
        self.return_type.to_c(c_buffer, path)?;
        c_buffer.write_char(' ')?;
        path.to_c(c_buffer, path)?;
        c_buffer.write_char('(')?;

        // Write the arguments
        for (i, arg) in self.args.iter().enumerate() {
            arg.to_c(c_buffer, path)?;
            if i + 1 != self.args.len() {
                c_buffer.write_str(", ")?;
            }
        }
        c_buffer.write_char(')')
    }
}

impl<'a> ToC for FP<'a> {
    fn to_c<W: Write>(&self, c_buffer: &mut W, path: &Path) -> Result<(), std::fmt::Error> {
        self.0.return_type.to_c(c_buffer, path)?;
        c_buffer.write_str(" (*")?;
        path.to_c(c_buffer, path)?;
        c_buffer.write_str(")(")?;

        // Write the arguments
        for (i, arg) in self.0.args.iter().enumerate() {
            arg.to_c(c_buffer, path)?;
            if i + 1 != self.0.args.len() {
                c_buffer.write_str(", ")?;
            }
        }

        c_buffer.write_str(")")
    }
}

impl ToC for Constant {
    fn to_c<W: Write>(&self, c_buffer: &mut W, path: &Path) -> Result<(), std::fmt::Error> {
        self.fn_repr().to_c(c_buffer, path)
    }
}

impl ToC for AssociatedItem {
    // NOTE: This function will write as a struct field
    //
    // Typedefs are ignored
    fn to_c<W: Write>(&self, c_buffer: &mut W, path: &Path) -> Result<(), std::fmt::Error> {
        match self {
            // Convert a constant into a function pointer
            AssociatedItem::Constant(x) => FP(&x.fn_repr()).to_c(c_buffer, path),
            // Convert a
            AssociatedItem::Function(x) => FP(x).to_c(c_buffer, path),
            // We ignore typedefs because they are just
            // sugar for roxu writers to handle
            AssociatedItem::Typedef(_) => Ok(()),
        }
    }
}

impl ToC for Trait {
    fn to_c<W: Write>(&self, c_buffer: &mut W, path: &Path) -> Result<(), std::fmt::Error> {
        // ----------------- //
        // Create the struct //
        // ----------------- //

        c_buffer.write_str("typedef struct ")?;
        path.to_c(c_buffer, path)?;
        c_buffer.write_str(" {\n")?;

        let mut new_path = Path::default();

        // Add the items
        for (key, item) in self.required_items.iter().chain(self.optional_items.iter()) {
            new_path.0.push(key.clone());
            item.to_c(c_buffer, &new_path)?;
            c_buffer.write_str(";\n")?;
            new_path.0.pop();
        }

        c_buffer.write_str("} ")?;
        path.to_c(c_buffer, path)?;
        c_buffer.write_str(";\n\n")?;

        // -------------------- //
        // Create the functions //
        // -------------------- //

        let mut new_path = path.clone();

        for (key, item) in self.required_items.iter().chain(self.optional_items.iter()) {
            if let Some(mut function) = item.fn_repr() {
                new_path.0.push(key.clone());

                // Create a self argument and prepend it
                let it = IdentType {
                    name: "self".to_string(),
                    typ: path.clone().into(),
                };
                function.args.insert(0, it);

                // Write the function
                function.to_c(c_buffer, &new_path)?;
                c_buffer.write_str(";\n")?;

                new_path.0.pop();
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use serde_json::json;

    #[test]
    fn test_write_path() -> Result<(), std::fmt::Error> {
        let mut b = String::new();
        let path = Path(vec!["roxu".into(), "primitives".into(), "u8".into()]);
        path.to_c(&mut b, &path)?;
        assert_eq!(b, "roxu__primitives__u8");

        Ok(())
    }

    #[test]
    fn test_write_item_type() -> Result<(), std::fmt::Error> {
        let mut b = String::new();
        let path = Path(vec!["roxu".into(), "primitives".into(), "u8".into()]);
        let identtype = IdentType::new("foo", path);
        let path2 = Path(vec!["roxu".into(), "random".into(), "place".into()]);
        identtype.to_c(&mut b, &path2)?;
        assert_eq!(b, "roxu__primitives__u8 foo");

        Ok(())
    }

    #[test]
    fn test_write_function_and_fp() -> Result<(), std::fmt::Error> {
        let mut b = String::new();

        let path = Path(vec!["test".into(), "add".into()]);

        let function: Function = serde_json::from_value(json! ({
            "return_type": { "path": ["roxu", "primitives", "u32"] },
            "args": [
                {
                    "name": "arg_a",
                    "type": { "path": ["roxu", "primitives", "u8"] },
                },
                {
                    "name": "arg_b",
                    "type": { "path": ["roxu", "primitives", "u16"] },
                }
            ]
        })).unwrap();

        function.to_c(&mut b, &path)?;

        assert_eq!(b, "roxu__primitives__u32 test__add(roxu__primitives__u8 arg_a, roxu__primitives__u16 arg_b)");

        let mut b = String::new();
        let fp = FP(&function);
        fp.to_c(&mut b, &path)?;

        assert_eq!(b, "roxu__primitives__u32 (*test__add)(roxu__primitives__u8 arg_a, roxu__primitives__u16 arg_b)");

        Ok(())
    }

    #[test]
    fn test_write_constant() -> Result<(), std::fmt::Error> {
        let mut b = String::new();
        let path = Path(vec!["roxu".into(), "primitives".into(), "u8".into()]);
        let constant: Constant = path.into();
        let path = Path(vec!["roxu".into(), "random".into(), "place".into()]);
        constant.to_c(&mut b, &path)?;
        assert_eq!(b, "roxu__primitives__u8 roxu__random__place()");

        Ok(())
    }

    #[test]
    fn test_write_trait() -> Result<(), std::fmt::Error> {
        let mut b = String::new();

        let path = Path(vec!["test".into(), "trait".into()]);

        let r#trait: Trait = serde_json::from_value(json!({
            "required_items": {
                "a": {
                    "function": {
                        "return_type": { "path": ["roxu", "primitives", "u8"] },
                        "args": [
                            {
                                "name": "arg1",
                                "type": { "path": ["roxu", "primitives", "u16"] },
                            },
                            {
                                "name": "arg2",
                                "type": { "path": ["roxu", "primitives", "u32"] },
                            }
                        ],
                    }
                },
                "c": {
                    "function": {
                        "return_type": { "path": ["roxu", "primitives", "u8"] },
                        "args": [
                            {
                                "name": "arg1",
                                "type": { "path": ["roxu", "primitives", "u16"] },
                            },
                            {
                                "name": "arg2",
                                "type": { "path": ["roxu", "primitives", "u32"] },
                            }
                        ],
                    }
                }
            },
            "optional_items": {
                "b": {
                    "constant": {
                        "type": { "path": ["roxu", "primitives", "u8"] },
                    }
                },
            }
        })).unwrap();

        r#trait.to_c(&mut b, &path)?;

        let expected = "typedef struct test__trait {\n\
        roxu__primitives__u8 (*a)(roxu__primitives__u16 arg1, roxu__primitives__u32 arg2);\n\
        roxu__primitives__u8 (*c)(roxu__primitives__u16 arg1, roxu__primitives__u32 arg2);\n\
        roxu__primitives__u8 (*b)();\n\
        } test__trait;\n\
        \n\
        roxu__primitives__u8 test__trait__a(test__trait self, roxu__primitives__u16 arg1, roxu__primitives__u32 arg2);\n\
        roxu__primitives__u8 test__trait__c(test__trait self, roxu__primitives__u16 arg1, roxu__primitives__u32 arg2);\n\
        roxu__primitives__u8 test__trait__b(test__trait self);\n\
        ";

        assert_eq!(b, expected);

        Ok(())
    }
}
