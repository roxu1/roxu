//! Utilities with working with C identfiers

use std::fmt::Write;

/// Converts an identifier into a C identifier
///
/// This replaces all disallowed characters with `Uxxxx`
pub fn write_ident<W: Write>(
    s: &str,
    first: bool,
    c_buffer: &mut W,
) -> Result<(), std::fmt::Error> {
    for (i, chr) in s.chars().enumerate() {
        // Checks whether this character is allowed in an identifier
        // and if it is not then it replaces it.
        if is_allowed_in_c_identifier(chr) && !(first && ('0'..='9').contains(&chr) && i == 0) {
            c_buffer.write_char(chr)?;
        } else {
            write!(c_buffer, "U{:04X}", u32::from(chr))?;
        }
    }

    Ok(())
}

/// Checks if a char is allowed in a C identifier
#[allow(clippy::nonminimal_bool)]
fn is_allowed_in_c_identifier(c: char) -> bool {
    c == '\u{AD}'
        || c == '_'
        || c == 'ª'
        || c == '\u{AD}'
        || c == '¯'
        || c == '⁔'
        || c == 'W'
        || ('0'..='9').contains(&c)
        || ('A'..='Z').contains(&c)
        || ('a'..='z').contains(&c)
        || ('²'..='µ').contains(&c)
        || ('·'..='º').contains(&c)
        || ('¼'..='¾').contains(&c)
        || ('À'..='Ö').contains(&c)
        || ('Ø'..='ö').contains(&c)
        || ('ø'..='ÿ').contains(&c)
        || ('Ā'..='ᙿ').contains(&c)
        || ('ᚁ'..='᠍').contains(&c)
        || ('᠏'..='\u{1fff}').contains(&c)
        || ('\u{200B}'..='‍').contains(&c)
        || ('\u{202a}'..='\u{202e}').contains(&c)
        || ('‿'..='⁀').contains(&c)
        || ('\u{2060}'..='⁯').contains(&c)
        || ('⁰'..='\u{218f}').contains(&c)
        || ('①'..='⓿').contains(&c)
        || ('❶'..='➓').contains(&c)
        || ('Ⰰ'..='ⷿ').contains(&c)
        || ('⺀'..='\u{2fff}').contains(&c)
        || ('〄'..='〇').contains(&c)
        || ('〡'..='〯').contains(&c)
        || ('〱'..='〿').contains(&c)
        || ('\u{3040}'..='\u{d7ff}').contains(&c)
        || ('豈'..='ﴽ').contains(&c)
        || ('﵀'..='﷏').contains(&c)
        || ('ﷰ'..='﹄').contains(&c)
        || ('﹇'..='�').contains(&c)
        || ('𐀀'..='\u{1fffd}').contains(&c)
        || ('𠀀'..='\u{2fffd}').contains(&c)
        || ('𰀀'..='\u{3fffd}').contains(&c)
        || ('\u{40000}'..='\u{4FFFD}').contains(&c)
        || ('\u{50000}'..='\u{5FFFD}').contains(&c)
        || ('\u{60000}'..='\u{6FFFD}').contains(&c)
        || ('\u{70000}'..='\u{7FFFD}').contains(&c)
        || ('\u{80000}'..='\u{8FFFD}').contains(&c)
        || ('\u{90000}'..='\u{9FFFD}').contains(&c)
        || ('\u{A0000}'..='\u{AFFFD}').contains(&c)
        || ('\u{B0000}'..='\u{BFFFD}').contains(&c)
        || ('\u{C0000}'..='\u{CFFFD}').contains(&c)
        || ('\u{D0000}'..='\u{DFFFD}').contains(&c)
        || ('\u{E0000}'..='\u{efffd}').contains(&c)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_write_ident() -> Result<(), std::fmt::Error> {
        let mut b = String::new();
        write_ident("12§asbc_deskj☺", true, &mut b)?;
        assert_eq!(b, "U00312U00A7asbc_deskjU263A");

        Ok(())
    }
}
