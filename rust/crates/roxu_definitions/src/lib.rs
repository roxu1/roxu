use std::collections::{HashMap, BTreeMap};

use serde::{Deserialize, Serialize};

pub type Items = HashMap<String, Item>;
pub type AssociatedItems = BTreeMap<String, AssociatedItem>;

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct RoxuFile {
    /// The version of roxu being used
    pub roxu_version: String,
    /// The name of the project being used
    pub name: String,
    /// The items in the project
    pub items: Items,
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone)]
#[serde(rename_all = "snake_case")]
pub enum Item {
    Module(Module),
    Function(Function),
    Trait(Trait),
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct Module {
    pub items: Items,
}

#[derive(Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Clone, Default)]
pub struct Path(pub Vec<String>);

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct Type {
    pub path: Path,
    #[serde(default)]
    pub applied_lifetimes: AppliedLifetimes,
    #[serde(default)]
    pub applied_generics: AppliedGenerics,
}

impl From<Path> for Type {
    fn from(path: Path) -> Self {
        Self { path, applied_lifetimes: Default::default(), applied_generics: Default::default()}
    }
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct Function {
    pub return_type: Type,
    pub args: Vec<IdentType>,
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct IdentType {
    pub name: String,
    #[serde(rename = "type")]
    pub typ: Type,
}

impl IdentType {
    /// Creates a new `IdentType`
    pub fn new<C: Into<String>, T: Into<Type>>(name: C, typ: T) -> Self {
        Self {
            name: name.into(),
            typ: typ.into(),
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct Constant {
    #[serde(rename = "type")]
    pub typ: Type,
}

impl Constant {
    pub fn fn_repr(&self) -> Function {
        Function {
            args: vec![],
            return_type: self.typ.clone()
        }
    }
}

impl<T> From<T> for Constant where Type: From<T> {
    fn from(typ: T) -> Self {
        Self { typ: typ.into() }
    }
}

pub type Generics = Vec<String>;
pub type Lifetimes = Vec<String>;
pub type AppliedGenerics = HashMap<String, Type>;
pub type AppliedLifetimes = HashMap<String, String>;

/// A typedef, equivilent to C `typedef` or rust `type =`
#[derive(Serialize, Deserialize, PartialEq, Eq, Clone)]
#[serde(rename = "typedef")]
pub struct Typedef {
    /// The type which this is aliased to
    #[serde(rename = "type")]
    pub typ: Type,
    /// The generics applied on this type
    #[serde(default)]
    pub applied_generics: AppliedGenerics,
    /// The lifetimes applied on this type
    #[serde(default)]
    pub applied_lifetimes: AppliedLifetimes,
}

/// An item which can be associated to a trait or type
#[derive(Serialize, Deserialize, PartialEq, Eq, Clone)]
#[serde(rename_all = "snake_case")]
pub enum AssociatedItem {
    Constant(Constant),
    Function(Function),
    Typedef(Typedef),
}

impl AssociatedItem {
    /// Gets the function representation of this AssociatedItem
    pub fn fn_repr(&self) -> Option<Function> {
        match self {
            AssociatedItem::Constant(x) => Some(x.fn_repr()),
            AssociatedItem::Function(x) => Some(x.clone()),
            AssociatedItem::Typedef(_) => None,
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct TraitType {
    pub path: Path,
    pub applied_lifetimes: AppliedLifetimes,
    pub applied_generics: AppliedGenerics,
}

impl From<Type> for TraitType {
    fn from(value: Type) -> Self {
        Self {
            path: value.path,
            applied_generics: value.applied_generics,
            applied_lifetimes: value.applied_lifetimes,
        }
    }
}

impl From<TraitType> for Type {
    fn from(value: TraitType) -> Self {
        Self {
            path: value.path,
            applied_generics: value.applied_generics,
            applied_lifetimes: value.applied_lifetimes,
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone, Default)]
pub struct Trait {
    #[serde(default)]
    pub subtraits: Vec<TraitType>,
    #[serde(default)]
    pub generics: Generics,
    #[serde(default)]
    pub lifetimes: Lifetimes,
    pub required_items: AssociatedItems,
    pub optional_items: AssociatedItems,
}
